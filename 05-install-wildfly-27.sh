#!/bin/bash
echo "Start script execution---->>05-install-wildfly-27.sh<----"

wget -nv https://github.com/wildfly/wildfly/releases/download/27.0.1.Final/wildfly-27.0.1.Final.tar.gz -O /tmp/wildfly-27.0.1.Final.tgz && \
tar xzf /tmp/wildfly-27.0.1.Final.tgz -C /opt && \
mv /opt/wildfly-27.0.1.Final /opt/wildfly && \
rm /tmp/wildfly-27.0.1.Final.tgz

echo "End script execution---->>05-install-wildfly-27.sh<<----"